package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/AmeyaRao98/wiki-hoe/api"
	"gitlab.com/AmeyaRao98/wiki-hoe/twitter"
)

func main() {
	lambda.Start(postNewTweet)
}

func postNewTweet() {
	rapidAPIClient := &http.Client{Timeout: time.Second * 10}

	twitterClient, err := api.GetTwitterClient(&api.TwitterCredentials{
		AccessToken:       os.Getenv("ACCESS_TOKEN"),
		AccessTokenSecret: os.Getenv("ACCESS_TOKEN_SECRET"),
		ConsumerKey:       os.Getenv("CONSUMER_KEY"),
		ConsumerSecret:    os.Getenv("CONSUMER_SECRET")})

	if err != nil {
		panic(err)
	}

	jokeChannel := api.GetJoke(rapidAPIClient)
	wikihowChannel := api.GetWikihowImage(rapidAPIClient)

	wikihow := <-wikihowChannel
	if wikihow.Error != nil {
		log.Print("Error fetching wikihow image:" + wikihow.Error.Error())
		return
	}

	mediaID, err := api.UploadWikihowImage(wikihow.Image, twitterClient)
	if err != nil {
		log.Print("Error uploading media:" + err.Error())
		return
	}

	joke := <-jokeChannel
	if joke.Error != nil {
		log.Print("Error fetching joke:" + joke.Error.Error())
		return
	}

	err = api.Tweet(joke.Joke, twitterClient, &twitter.StatusUpdateParams{MediaIds: []int64{*mediaID}})
	if err != nil {
		log.Print("Error tweeting tweet:" + err.Error())
		return
	}
}
