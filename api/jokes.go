package api

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
)

type jokeResponse struct {
	ID        string `json:"id"`
	Content   string `json:"content"`
	NSFW      bool   `json:"nsfw"`
	Upvotes   int    `json:"upvotes"`
	Downvotes int    `json:"downvotes"`
}

// JokeRoutineResponse stores the response from GetJoke to send through the channel
type JokeRoutineResponse struct {
	Joke  *string
	Error error
}

// GetJoke gets and returns a random joke from the joke API
func GetJoke(client *http.Client) <-chan JokeRoutineResponse {
	c := make(chan JokeRoutineResponse)
	go func() {
		req, _ := http.NewRequest("GET", "https://joke3.p.rapidapi.com/v1/joke?nsfw=true", nil)
		req.Header.Add("x-rapidapi-key", os.Getenv("X_RAPIDAPI_KEY"))
		req.Header.Add("x-rapidapi-host", "joke3.p.rapidapi.com")
		res, err := client.Do(req)
		if err != nil {
			c <- JokeRoutineResponse{nil, err}
			return
		}
		decoder := json.NewDecoder(res.Body)
		var jokeData jokeResponse
		decoder.Decode(&jokeData)
		log.Printf(`Fetched joke "%v"`, jokeData.Content)
		c <- JokeRoutineResponse{&jokeData.Content, nil}
	}()
	return c
}
