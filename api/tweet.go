package api

import (
	"errors"
	"log"

	"github.com/dghubble/oauth1"
	"gitlab.com/AmeyaRao98/wiki-hoe/twitter"
)

// TwitterCredentials stores all of our access/consumer tokens
// and secret keys needed for authentication against
// the twitter REST API.
type TwitterCredentials struct {
	ConsumerKey       string
	ConsumerSecret    string
	AccessToken       string
	AccessTokenSecret string
}

// GetTwitterClient is a helper function that will return a twitter client
func GetTwitterClient(creds *TwitterCredentials) (*twitter.Client, error) {
	config := oauth1.NewConfig(creds.ConsumerKey, creds.ConsumerSecret)
	token := oauth1.NewToken(creds.AccessToken, creds.AccessTokenSecret)

	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)

	_, _, err := client.Accounts.VerifyCredentials(&twitter.AccountVerifyParams{
		SkipStatus:   twitter.Bool(true),
		IncludeEmail: twitter.Bool(true),
	})
	if err != nil {
		return nil, err
	}

	return client, nil
}

// Tweet tweets
func Tweet(content *string, client *twitter.Client, params *twitter.StatusUpdateParams) error {
	_, _, err := client.Statuses.Update(*content, params)
	if err != nil {
		return err
	}
	log.Printf(`Tweeted joke "%v" with image %v`, *content, params.MediaIds[0])
	return nil
}

// UploadWikihowImage uploads an image in byte slice format and returns the media id
func UploadWikihowImage(image []byte, client *twitter.Client) (*int64, error) {
	res, _, err := client.Media.Upload(image, "image/jpeg")
	if err != nil {
		return nil, err
	}
	if res.MediaID <= 0 {
		return nil, errors.New("failed to uplaod image")
	}
	log.Printf(`Uploaded media %v`, res.MediaID)
	return &res.MediaID, nil
}
