package api

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type wikihowResponse struct {
	URL string `json:"1"`
}

// WikihowRoutineResponse stores the response from GetWikihowImage to send through the channel
type WikihowRoutineResponse struct {
	Image []byte
	Error error
}

// GetWikihowImage gets and returns a random wikihow image as a byte slice
func GetWikihowImage(client *http.Client) <-chan WikihowRoutineResponse {
	c := make(chan WikihowRoutineResponse)
	go func() {
		req, _ := http.NewRequest("GET", "https://hargrimm-wikihow-v1.p.rapidapi.com/images?count=1", nil)
		req.Header.Add("x-rapidapi-key", os.Getenv("X_RAPIDAPI_KEY"))
		req.Header.Add("x-rapidapi-host", "hargrimm-wikihow-v1.p.rapidapi.com")
		res, err := client.Do(req)
		if err != nil {
			c <- WikihowRoutineResponse{nil, err}
			return
		}
		decoder := json.NewDecoder(res.Body)
		var wikiData wikihowResponse
		decoder.Decode(&wikiData)
		res, err = client.Get(wikiData.URL)
		if err != nil {
			c <- WikihowRoutineResponse{nil, err}
			return
		}
		defer res.Body.Close()
		imgBytes, err := ioutil.ReadAll(res.Body)
		if err != nil {
			c <- WikihowRoutineResponse{nil, err}
			return
		}
		log.Printf(`Fetched wikihow image "%v"`, wikiData.URL)
		c <- WikihowRoutineResponse{imgBytes, nil}
	}()
	return c
}
