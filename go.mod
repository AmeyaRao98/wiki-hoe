module gitlab.com/AmeyaRao98/wiki-hoe

go 1.15

require (
	github.com/aws/aws-lambda-go v1.22.0
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/dghubble/oauth1 v0.7.0
	github.com/dghubble/sling v1.3.0
)
